"""
Script for one electron s-orbital integrals
"""
import numpy as np
import matplotlib.pyplot as plt


def dist(v):
    """Returns an n x n distance matrix from a 3 x n vector."""
    return np.linalg.norm(np.subtract.outer(v, v), axis=0)


def oprod(v):
    """Returns an open product of a vector with itself."""
    return np.outer(v, v)


def osum(v):
    """Returns an open sum of a vector with itself."""
    return np.add.outer(v, v)


def reduced_v(v):
    """Returns a n x n reduced value matrix from a n-length vector."""
    return oprod(v) / osum(v)


def sgauss_norm(alpha):
    """Returns the norm(s) of s-type Gaussian basis function(s)."""
    return (np.pi / (2 * alpha))**(3./4.)


def sgauss_overlap(alpha, r):
    """Returns the overlap matrix of s-type Gaussian basis functions."""
    vnorm = sgauss_norm(alpha)
    norm = oprod(vnorm) * (np.pi / osum(alpha))**(3./2.)
    exp = osum(alpha) * dist(r)**2
    return norm * np.exp(-exp)


def sgauss_kinetic(alpha, r):
    """Returns the kinetic energy matrix of s-type Gaussian basis
    functions."""
    olap = sgauss_overlap(alpha, r)
    reda = reduced_v(alpha)
    return olap * (-2 * (reda*dist)**2 + 3 * reda)


def sgauss_potential(alpha, r, rnuc, Znuc):
    """Returns the potential energy matrix of s-type Gaussian basis
    functions."""
    olap = sgauss_overlap(alpha, r)
    avr = osum(alpha*r) / osum(alpha)
    rdist = np.linalg.norm(avr - rnuc, axis=0)
    V = 0
    for ZA, rA in zip(Znuc, rdist): # this part is probably not right...
        if rA < 1e-10:
            V -= 2 * ZA * olap * np.sqrt(osum(alpha) / np.pi)
        else:
            V -= ZA * olap * sp.erf(np.sqrt(osum(alpha) * rA)) / rA


def main():
    """The main routine."""
    pass


if __name__ == '__main__':
    main()
