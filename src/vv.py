"""
Script for propagating trajectories on a single surface.
"""
import sys
import numpy as np
import lightspeed as ls
import psiw


class VelocityVerlet(object):
    """
    The velocity verlet class object.
    """
    def __init__(self, xi=None, pi=None, Vi=None, fi=None, m=None, dt=10):
        self.dt = dt
        self.xi = xi
        self.pi = pi
        self.fi = fi
        self.Vi = Vi
        self.m = m

        # propagation properties
        self.t = 0
        self.x = self.xi
        self.p = self.pi

        # energetic properties
        self.V = self.Vi
        self.f = self.fi

    @property
    def T(self):
        """Calculates the classical kinetic energy."""
        return np.sum(self.p**2 / (2 * self.m))

    @property
    def E(self):
        """Calculates the classical total energy."""
        return self.T + self.V

    def propagate_x(self, dt):
        """Propagates the molecular position."""
        self.x += (self.p / self.m) * dt + 0.5*(self.f / self.m) * dt**2

    def propagate_phalf(self, dt):
        """Propagates the molecular momentum."""
        self.p += 0.5*self.f * dt

    def step(self, new_V, new_grad):
        """Take one step forward with new potentials and gradients."""
        self.propagate_x(self.dt)
        self.propagate_phalf(self.dt)
        self.V = new_V
        self.f = -new_grad
        self.propagate_phalf(self.dt)
        self.t += self.dt


def harmV(x, freq=1):
    """Returns the potential of a harmonic oscillator."""
    return 0.5 * freq * x**2


def dharmVdt(x, freq=1):
    """Returns the gradient of a harmonic oscillator potential."""
    return freq * x


def test_harmonic():
    """Tests VelocityVerlet with a harmonic oscillator."""
    nstep = 1000

    xi = 0.
    pi = 0.2
    m = 2000.
    Vi = harmV(xi, freq=1/(2*m))
    fi = -dharmVdt(xi, freq=1/(2*m))

    myvv = VelocityVerlet(xi, pi, Vi, fi, m)
    myvv.step(Vi, -fi)

    print(myvv.E)

    for i in range(nstep):
        Vt = harmV(myvv.x, freq=1/(2*m))
        grad = dharmVdt(myvv.x, freq=1/(2*m))
        myvv.step(Vt, grad)

    print(myvv.E)


def main():
    """The main routine."""
    nstep = 100
    fname = '../tests/x.xyz'

    resources = ls.ResourceList.build()
    mol = ls.Molecule.from_xyz_file(fname)
    geom = psiw.Geometry.build(resources=resources, molecule=molecule,
                               basisname='6-31gss', basisspherical=False)
    ref = psiw.RHF.from_options(geometry=geometry, g_convergence=1e-7,
                                fomo=True, fomo_method='gaussian',
                                fomo_temp=0.2, fomo_nocc=39, fomo_nact=2)
    cas = psiw.CASCI.from_options(reference=ref, nocc=39, nact=2, nalpha=1,
                                  nbeta=1, S_inds=[0], S_nstates=[2])
    lot = psiw.CASCI_LOT.from_options(casci=casci, print_level=1,
                                      rhf_guess=True, rhf_mom=True)

    myvv = VelocityVerlet(xi, pi, Vi, fi, m)
    myvv.step(Vi, -fi)

    for i in range(nstep):
        pass


if __name__ == '__main__':
    main()
