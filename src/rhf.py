"""
Script for running RHF using Lightspeed.
"""
import sys
import numpy as np
import lightspeed as ls


class RestrictedHartreeFock(object):
    """
    The class object for RHF calculations and data
    """
    def __init__(self, fname=None, bname=None, mbname=None,
                 nimax=50, dEthresh=1e-6, Gthresh=1e-5):
        self.fname = fname
        self.bname = bname
        self.mbname = mbname
        self.nimax = nimax
        self.dEthresh = dEthresh
        self.Gthresh = Gthresh

        # resources
        self.resources = ls.ResourceList.build()

        # molecule properties
        self.mol = None
        self.nelec = 0
        self.nalpha = 0
        if self.fname is not None:
            self.import_molecule(self.fname)

        # basis properties
        self.bas = None
        self.nbas = 0
        self.pairlist = None
        if self.bname is not None:
            self.import_basis(self.bname)

        # minimal basis properties
        self.minbas = None
        self.nminbas = 0
        if self.mbname is not None:
            self.import_minbasis(self.mbname)

        # energies
        self.Enuc = 0
        self.Erhf = 0
        self.ep = None

        # matrices
        self.S = None
        self.T = None
        self.V = None
        self.H = None

        self.F = None
        self.J = None
        self.K = None

        self.X = None
        self.C = None
        self.D = None

        # iteration properties
        self.diis = None
        self.G = None
        self.dE = 0
        self.maxG = 0

    def import_molecule(self, fname):
        """Imports a molecular geometry from an XYZ file."""
        print('Importing XYZ file ' + fname)
        self.mol = ls.Molecule.from_xyz_file(fname)
        self.nelec = int(self.mol.nuclear_charge - self.mol.charge)
        self.nalpha = self.nelec // 2
        print('Number of atoms: {:d}'.format(self.mol.natom))
        print('Number of electrons: {:d}\n'.format(self.nelec))

    def import_basis(self, bname, thresh=1e-14):
        """Imports an atomic basis from the basis name."""
        print('Importing basis ' + bname)
        self.bas = ls.Basis.from_gbs_file(self.mol, bname)
        self.nbas = self.bas.nao
        print('Number of basis functions: {:d}\n'.format(self.nbas))
        self.pairlist = ls.PairList.build_schwarz(self.bas, self.bas, True,
                                                  thresh)

    def import_minbasis(self, bname):
        """Imports a minimal atomic basis from the basis name."""
        print('Importing minimal basis ' + bname)
        self.minbas = ls.Basis.from_gbs_file(self.mol, bname)
        self.nminbas = self.minbas.nao
        print('Number of minimal basis functions: {:d}\n'.format(self.nminbas))

    def compute_energy(self, dthresh=1e-6, sthresh=1e-14):
        """Computes an RHF energy for the specified molecule and basis."""
        print('Starting RHF energy calculation')
        print('-------------------------------\n')

        # compute the nuclear repulsion energy
        self.Enuc = self.mol.nuclear_repulsion_energy()
        print('Nuclear repulsion energy: {:16.8f}\n'.format(self.Enuc))

        # compute one electron integral matrices
        self.S = ls.IntBox.overlap(self.resources, self.pairlist)
        self.T = ls.IntBox.kinetic(self.resources, self.pairlist)
        self.V = ls.IntBox.potential(self.resources, ls.Ewald.coulomb(),
                                     self.pairlist, self.mol.xyzZ)
        self.H = ls.Tensor.zeros_like(self.T)
        self.H[...] = self.T.np[...] + self.V.np[...]

        # orthogonalize the overlap matrix
        self.X = ls.Tensor.canonical_orthogonalize(self.S, 1e-6)
        col_size = self.X.shape[0]

        # form the guess SAD orbitals
        self.C = ls.SAD.orbitals(self.resources, self.mol, self.bas,
                                 self.minbas)
        self.D = ls.Tensor.chain([self.C, self.C], [False, True])

        # build a DIIS object
        self.diis = ls.DIIS(6)

        # start RHF iterations
        print('Starting RHF iterations\n')
        print('{:>4s}{:>16s}{:>16s}{:>16s}'.format('iter', 'E_RHF', 'dE',
                                                   'max(G)'))
        self.F = ls.Tensor.zeros_like(self.H)
        for i in range(self.nimax):
            # compute Coulomb and exchange integrals
            self.J = ls.IntBox.coulomb(self.resources, ls.Ewald.coulomb(),
                                       self.pairlist, self.pairlist, self.D,
                                       dthresh, sthresh)
            self.K = ls.IntBox.exchange(self.resources, ls.Ewald.coulomb(),
                                        self.pairlist, self.pairlist, self.D,
                                        True, dthresh, sthresh)

            # build the Fock matrix
            self.F[...] = self.H.np[...] + 2*self.J.np[...] - self.K.np[...]

            # calculate E_RHF
            HF = ls.Tensor.zeros_like(self.H)
            HF[...] = self.H.np[...] + self.F.np[...]
            self.Erhf = self.Enuc + self.D.vector_dot(HF)

            # calculate the orbital gradient matrix
            SDF = ls.Tensor.chain([self.S, self.D, self.F],
                                  [False, False, False])
            FDS = ls.Tensor.chain([self.F, self.D, self.S],
                                  [False, False, False])
            SDFFDS = ls.Tensor.zeros_like(self.H)
            SDFFDS[...] = SDF.np[...] - FDS.np[...]
            self.G = ls.Tensor.chain([self.X, SDFFDS, self.X],
                                     [True, False, False])
            self.maxG = np.max(np.abs(self.G))

            # test the change in energy and the maximum gradient value for
            # convergence
            if i > 0:
                self.dE = np.abs(self.Erhf - Elast)
                print('{:4d}{:16.8f}{:16.8f}{:16.8f}'.format(i, self.Erhf,
                                                             self.dE,
                                                             self.maxG))
                if self.dE < self.dEthresh and self.maxG < self.Gthresh:
                    print('\nConvergence critera achieved on iteration ' \
                          '{:d}'.format(i))
                    break
            else:
                print('{:4d}{:16.8f}{:>16s}{:16.8f}'.format(i, self.Erhf,
                                                            '----', self.maxG))

            # save the RHF energy for the next iteration
            Elast = self.Erhf

            # propagate the Fock matrix
            self.F = self.diis.iterate(self.F, self.G)

            # calculate the new density matrix
            Fp = ls.Tensor.chain([self.X, self.F, self.X],
                                 [True, False, False])
            self.ep, U = ls.Tensor.eigh(Fp)
            self.C = ls.Tensor.chain([self.X, U], [False, False])
            Cocc = ls.Tensor.array(self.C[:,:self.nalpha])
            self.D = ls.Tensor.chain([Cocc, Cocc], [False, True])
        else:
            raise ValueError('Maximum number of RHF iterations exceeded')

        print('Final RHF energy: {:16.8f}\n'.format(self.Erhf))

        print('------------')
        print('Finished RHF')


def main():
    """The main routine."""
    xyz_fname = sys.argv[1]
    bas_name = sys.argv[2]
    mbas_name = 'cc-pvdz-minao'
    rhf = RestrictedHartreeFock(xyz_fname, bas_name, mbas_name)
    rhf.compute_energy()
    print('Somehow nothing failed.')


if __name__ == '__main__':
    main()
