"""
Script for computing the configuration interaction singles energies
and properties
"""
import sys
import numpy as np
import lightspeed as ls
import rhf


class ConfigurationInteractionSingles(object):
    """
    The CIS class object.
    """
    def __init__(self, rhfobj=None, nstates=2, nsub=10, ndav=50):
        self.rhfobj = rhfobj
        self.nstates = nstates
        self.nsub = nsub
        self.ndav = ndav

        # energy properties
        self.Erhf = 0

        # electron properties
        self.nocc = 0
        self.nvir = 0
        self.ep = None

        # matrices
        self.C = None
        self.Cocc = None
        self.Cvir = None
        self.P = None
        self.b = None
        self.sigma = None

    def set_rhf(self, rhfobj):
        """Sets the RHF object."""
        self.rhfobj = rhfobj

    def compute_rhf(self):
        """Calculates the RHF energy."""
        self.rhfobj.compute_energy()

        # set electron properties
        self.nocc = self.rhfobj.nalpha
        self.nvir = self.rhfobj.nbas - self.nocc

        # set matrices
        self.Erhf = self.rhfobj.Erhf
        self.C = self.rhfobj.C
        self.Cocc = ls.Tensor.array(self.C[:,:self.nocc])
        self.Cvir = ls.Tensor.array(self.C[:,:self.nvir])
        self.ep = self.rhfobj.ep

    def compute_cis(self, dthresh=1e-6, sthresh=1e-14):
        """Calculates the CIS energy."""
        print('\nStarting CIS energy calculation')
        print('-------------------------------\n')
        # find orbital energy differences
        epocc = self.ep[:self.nocc]
        epvir = self.ep[self.nocc:]
        epocc, epvir = np.meshgrid(epocc, epvir, indexing='ij')
        self.P = ls.Tensor.array(epvir - epocc)

        # find b Guess matrix from smallest nstates energy differences
        iflat = np.argpartition(self.P, self.nstates-1, axis=None)
        ind = np.unravel_index(iflat[:self.nstates], self.P.shape)
        self.b = [ls.Tensor.zeros_like(self.P) for i in range(self.nstates)]
        for i in range(self.nstates):
            self.b[i][ind[0][i], ind[1][i]] = 1

        # setup the Davidson object
        self.dav = ls.Davidson(self.nstates, self.nsub, 1e-6, 1e-6)

        # begin Davidson iterations
        print('Starting Davidson iterations\n')
        for i in range(self.ndav):
            print('Davidson iteration {:d}'.format(i))
            # build sigma values
            self.sigma = [ls.Tensor.zeros_like(self.b[k]) for k in
                          range(len(self.b))]
            for j in range(len(self.b)):
                self.sigma[j][...] = self.P.np[...] * self.b[j].np[...]
                D = ls.Tensor.chain([self.Cocc, self.b[j], self.Cvir],
                                    [False, False, True])
                J = ls.IntBox.coulomb(self.rhfobj.resources, ls.Ewald.coulomb(),
                                      self.rhfobj.pairlist, self.rhfobj.pairlist,
                                      D, dthresh, sthresh)
                K = ls.IntBox.exchange(self.rhfobj.resources, ls.Ewald.coulomb(),
                                       self.rhfobj.pairlist, self.rhfobj.pairlist,
                                       D, False, dthresh, sthresh)
                JK = ls.Tensor.zeros_like(J)
                JK[...] = 2 * J.np[...] - K.np[...]
                self.sigma[j][...] += ls.Tensor.chain([self.Cocc, JK, self.Cvir],
                                                      [True, False, False])

            rs, ldas = self.dav.add_vectors(self.b, self.sigma)

            # check for convergence
            if self.dav.is_converged:
                print('\nDavidson converged on iteration {:d}\n'.format(i))
                break

            # calculate d
            for r, l in zip(rs, ldas):
                r[...] /= -(self.P[...]  - l)

            # get new trial vectors
            self.b = self.dav.add_preconditioned(rs)
        else:
            raise ValueError('Maximum number of Davidson iterations exceeded.')

        print('------------')
        print('Finished CIS\n')


def main():
    """The main routine."""
    xyz_fname = '../tests/water.xyz'
    bas_name = 'cc-pvdz'
    mbas_name = 'cc-pvdz-minao'
    myrhf = rhf.RestrictedHartreeFock(xyz_fname, bas_name, mbas_name)
    mycis = ConfigurationInteractionSingles(myrhf, 3)
    mycis.compute_rhf()
    mycis.compute_cis()
    print('Somehow nothing failed.')


if __name__ == '__main__':
    main()
